//
//  Obstacle.swift
//  HoppyBunny
//
//  Created by Jacob Haff on 6/24/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

import Foundation

class Obstacle : CCNode {
    weak var topCarrot : CCNode!
    weak var bottomCarrot : CCNode!
    weak var goal : CCNode!
    weak var theNode : CCNode!

    
    let topCarrotMinimumPositionY : CGFloat = 128
    let bottomCarrotMaximumPositionY : CGFloat = 440
    let carrotDistance : CGFloat = 120
    
    func setupRandomPosition() {
        let randomPrecision : UInt32 = 100
        let random = CGFloat(arc4random_uniform(randomPrecision)) / CGFloat(randomPrecision)
        let range = bottomCarrotMaximumPositionY - carrotDistance - topCarrotMinimumPositionY
        topCarrot.position = ccp(topCarrot.position.x, topCarrotMinimumPositionY + (random * range));
        bottomCarrot.position = ccp(bottomCarrot.position.x, topCarrot.position.y + carrotDistance);
        goal.position = bottomCarrot.positionInPoints
    }
    
    func didLoadFromCCB() {
        topCarrot.physicsBody.sensor = false
        bottomCarrot.physicsBody.sensor = false
        setupRandomPosition()
//        goal.physicsBody.sensor = true

    }
}