import Foundation
import GameKit
import StoreKit


class MainScene: CCNode {
//    weak var projectile: CCNode!
//    weak var circle: CCNode!
//    weak var square: CCNode!
    var points: NSInteger = 0
    var ammo: NSInteger = 0
    var timesPlayed: NSInteger = 0
    var highScore:NSInteger = 0
    var reload:Float = 1000
    var reloadMax:Float = 1000
    var musicActivated:Bool = true
    var soundActivated:Bool = true





    weak var scoreLabel: CCLabelTTF!
    weak var scoreLabel2: CCLabelTTF!
    weak var reloadBar: CCNodeColor!
    weak var launchButton: CCButton!
    weak var obstaclePlacerNode: CCNode!
    weak var groundedNode: CCNode!
    weak var obstacle: Obstacle!

    weak var launchHere: CCNode!
    weak var rewardSprite: CCSprite!
    weak var hand248: CCSprite!


//    weak var gameOverAnimation: CAAnimation!


    weak var fire:CCParticleSystem!


    weak var highScoreLabel: CCLabelTTF!
    weak var highScoreLabel2: CCLabelTTF!


    var gameOver: Bool! = true

//    let mixpanel: Mixpanel = Mixpanel.sharedInstance()


    
    
    weak var gamePhysicsNode: CCPhysicsNode!
    weak var goal: CCNode!
    weak var oval: CCNode!
//    weak var obstacle2: Obstacle!


    weak var projectileRemover: CCNode!

    static var GAME_STATE_HIGHSCORE_KEY: NSString = "highScore"
    static var GAME_STATE_TIMESPLAYED_KEY: NSString = "timesPlayed"
    
    var music: OALSimpleAudio = OALSimpleAudio.sharedInstance()
    var owSound: OALSimpleAudio = OALSimpleAudio.sharedInstance()
    var pointSound: OALSimpleAudio = OALSimpleAudio.sharedInstance()


    
    
    func didLoadFromCCB() {
//groundedNode.physicsBody.sensor = true
gamePhysicsNode.debugDraw = false
//        obstacle = CCBReader.load("Obstacle") as! Obstacle

        music.preloadBg("Song.mp3")
        owSound.preloadEffect("Die.mp3")
        pointSound.preloadEffect("Point.mp3")

        oval.rotation = 0
        
//        GameCenterInteractor.sharedInstance.authenticationCheck()
        
        timesPlayed = NSUserDefaults.standardUserDefaults().integerForKey("timesPlayed")
        
//        iAdHandler.sharedInstance.loadAds(bannerPosition: BannerPosition.Bottom)
        iAdHandler.sharedInstance.loadInterstitialAd()
       highScore = NSUserDefaults.standardUserDefaults().integerForKey("highScore")
        highScoreLabel.string = "Best:" + String(highScore)
        highScoreLabel2.string = "Best:" + String(highScore)
        iAdHandler.sharedInstance.displayInterstitialAd()

        self.animationManager.runAnimationsForSequenceNamed("titleScreen")
        userInteractionEnabled = true
//        gamePhysicsNode.collisionDelegate = self
        

//        schedule("spawnBlocks", interval: (2))
//        goal.physicsBody.sensor = true
    }
//    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, circle nodeA: CCNode!, goal: CCNode!) -> ObjCBool {
////        goal.removeFromParent()
//        points++
//        scoreLabel.string = String(points)
//        spawnNewObstacle()
//
//        return true
//    }
    
//    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, circle: CCNode!, goal: CCNode!) -> ObjCBool {
//        println("TODO: handle Game Over")
//        
//        
//        return true
//    }
    
    //lol
    func musicButton(){
        musicActivated = !musicActivated
    }
    
    func soundButton(){
        soundActivated = !soundActivated
    }
    
    func launch() {
//        showLeaderboard()
//        iAdHandler.sharedInstance.displayBannerAd()
//        iAdHandler.sharedInstance.displayInterstitialAd()
        if !gameOver {

        if (reload >= reloadMax) {
            reload = 0
        setupRandomPosition()
        //load in projectile
        let projectile = CCBReader.load("Projectile")
        //set its position to bottom-middle of screen
        projectile.position = launchButton.positionInPoints
        projectile.scale = 0.3
        gamePhysicsNode.addChild(projectile)
        //add it to the phys node
        projectile.physicsBody.velocity.y = 460
//give it a push!
            }
        }
    }
    
    func setupRandomPosition() {
        // returns a value between 0.f and 1.f
        let randomPrecision: UInt32 = 100
        let random = arc4random_uniform(4)
        // calculate the end of the range of top pipe
        NSLog("%i",random)
        
    }
    
    
    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, projectile: CCNode!, projectileRemover: CCNode!) -> ObjCBool {
        
        projectile.removeFromParent()
        
        
return true
    }
    
    /**
    When called, delays the running of code included in the `closure` parameter.
    
    - parameter delay:  how long, in milliseconds, to wait until the program should run the code in the closure statement
    */
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
   
//
//    func test() {
//        self.animationManager.animationDidStop(gameOverAnimation, finished: true)
//    }
//    func leaderboard() {
//        showLeaderboard()
//    }
//    func facebook() {
//        SharingHandler.sharedInstance.postToFacebook(postWithScreenshot: true)
//    }
//    func twitter() {
//        SharingHandler.sharedInstance.postToTwitter(stringToPost: "I just played the impossible yet addicting game #Be-Square!", postWithScreenshot: true)
//    }
    
    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, circle: CCNode!, level: CCNode!) -> ObjCBool {
//        circle.physicsBody.collisionType = "circle"

        if (!gameOver){
 
            if soundActivated{
                owSound.playEffect("Die.mp3", loop: false)
            }
//        owSound.playEffect("Die.mp3", loop: false)
        music.stopBg()

        
        
        
        circle.physicsBody.affectedByGravity = true
//        circle.physicsBody.sensor = true
            circle.physicsBody.collisionType = "circle"
//        goal.removeFromParent()
        

        
        NSLog("collide")
        if gameOver == false{
        self.animationManager.runAnimationsForSequenceNamed("gameOver")
            circle.animationManager.runAnimationsForSequenceNamed("youLose")
//            iAdHandler.sharedInstance.loadInterstitialAd()
            delay(0.5){
            if self.timesPlayed % 4 == 0{
                iAdHandler.sharedInstance.displayInterstitialAd()
//                self.mixpanel.track("AdDisplayAttempt")

                }
            }
            
            timesPlayed += 1
            NSUserDefaults.standardUserDefaults().setInteger(Int(timesPlayed), forKey: "timesPlayed")
            NSUserDefaults.standardUserDefaults().synchronize()

            if points < 8{
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "Circle2.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 0.5
                rewardSprite.scale = 0.15

            }
            else if points < 18 {
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "bronzestar.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 1.0
                rewardSprite.scale = 0.5


            }
            else if points < 25 {
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "silverstar.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 1.0
                rewardSprite.scale = 0.5


            }
            else {
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "goldstar.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 1.0
                rewardSprite.scale = 0.5


            }
            
            points = 0
            scoreLabel.string = String(0)
//            scoreLabel2.string = String(0)
            let move = CCActionEaseBounceOut(action: CCActionMoveBy(duration: 0.2, position: ccp(0, 4)))
            let moveBack = CCActionEaseBounceOut(action: move.reverse())
            let shakeSequence = CCActionSequence(array: [move, moveBack])
            runAction(shakeSequence)

        }
        
        gameOver = true
            
            delay(0.2 ){
                circle.removeFromParent()
            }
            
           

        
        }
        return true

    }
    func playFromCredits()
    {
        self.animationManager.runAnimationsForSequenceNamed("exitCreditsAndPlay")
        gameOver = false
        //        var randomWait = SKAction.waitForDuration(waitDuration)
        //        self.runAction(randomWait)
//        iAdHandler.sharedInstance.displayBannerAd()
        

    }
    
    func play(){
//        oval.physicsBody.angularVelocity = 1

        if musicActivated{
        music.playBg()
        }
    self.animationManager.runAnimationsForSequenceNamed("exitTitle")
gameOver = false
//        var randomWait = SKAction.waitForDuration(waitDuration)
//        self.runAction(randomWait)
//                iAdHandler.sharedInstance.displayBannerAd()
        

    }
    func ccPhysicsCollisionBegin(pair: CCPhysicsCollisionPair!, circle: CCNode!, goal: Obstacle!) -> ObjCBool {
        if !gameOver{
        NSLog("goal")
        points += 1
            delay(0.5){
            self.obstacle.animationManager.runAnimationsForSequenceNamed("chomp")
            }

        hand248.visible = false
        circle.physicsBody.collisionType = "goal"
        scheduleOnce(#selector(MainScene.spawnNewObstacle), delay: 0.7)
        

        if soundActivated{
        pointSound.playEffect("Point.mp3", loop: false)
        }
        if points > highScore {
            
//            GameCenterInteractor.sharedInstance.saveHighScore(Int(points))
            
            let yay: CCParticleSystem = CCBReader.load("Yay3") as! Shatter
            // make the particle effect clean itself up, once it is completed
            yay.autoRemoveOnFinish = true;
            
            //    CGPoint partPos = [ _gameplayNode convertToNodeSpace:(nodeA.position) ];
            // place the particle effect on the seals position
            yay.position = highScoreLabel.positionInPoints;
            // add the particle effect to the scene
            highScoreLabel.parent.addChild(yay)
            self.animationManager.runAnimationsForSequenceNamed("highScore")
            highScore = points
            NSUserDefaults.standardUserDefaults().setInteger(highScore, forKey: "highScore")
            highScoreLabel.string = "Best:" + String(highScore)
            highScoreLabel2.string = "Best:" + String(highScore)
            
            

        }
        
        else {
        self.animationManager.runAnimationsForSequenceNamed("score")
        let yay: CCParticleSystem = CCBReader.load("Yay") as! Shatter
        // make the particle effect clean itself up, once it is completed
        yay.autoRemoveOnFinish = true;
        
        //    CGPoint partPos = [ _gameplayNode convertToNodeSpace:(nodeA.position) ];
        // place the particle effect on the seals position
        yay.position = scoreLabel.positionInPoints;
        // add the particle effect to the scene
        scoreLabel.parent.addChild(yay)
        }

        
     
        
        let splash: CCParticleSystem = CCBReader.load("Boom") as! CCParticleSystem
        // make the particle effect clean itself up, once it is completed
//        splash.autoRemoveOnFinish = true;
        
        //    CGPoint partPos = [ _gameplayNode convertToNodeSpace:(nodeA.position) ];
        // place the particle effect on the seals position
        splash.position = circle.positionInPoints;
        // add the particle effect to the scene
        circle.parent.addChild(splash)
        
        scoreLabel.string = String(points)
        scoreLabel2.string = String(points)

        circle.physicsBody.affectedByGravity = true
        circle.physicsBody.sensor = true
//        circle.physicsBody.collisionType = nil
//        projectile.removeFromParent()
        NSLog("collide")
        
//        circle.animationManager.runAnimationsForSequenceNamed("die")
            delay(0.5){
                circle.removeFromParent()
            }

        
        }
        return false
    }
    
    
    
    func ccPhysicsCollisionPostSolve(pair: CCPhysicsCollisionPair!, circle: CCNode!, projectile: CCNode!) -> ObjCBool {
        
        if (!gameOver){
            
            delay(0.5){
                if self.timesPlayed % 3 == 0{
                    iAdHandler.sharedInstance.displayInterstitialAd()
//                    self.mixpanel.track("AdDisplayAttempt")
                    
                }
            }
            
            if soundActivated{
                owSound.playEffect("Die2.mp3", loop: false)
            }
        circle.animationManager.runAnimationsForSequenceNamed("youLose")
        scoreLabel.string = String(points)
            scoreLabel2.string = String(points)

        circle.physicsBody.affectedByGravity = true
        circle.physicsBody.sensor = true
        projectile.removeFromParent()
        NSLog("collide")
        gameOver = true
            if soundActivated{
                owSound.playEffect("Die.mp3", loop: false)
            }
        self.animationManager.runAnimationsForSequenceNamed("gameOver")
        points = 0
            
            if points < 10{
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "Circle2.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 0.5
                rewardSprite.scale = 0.15
                
            }
            else if points < 20 {
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "bronzestar.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 1.0
                rewardSprite.scale = 0.7
                
                
            }
            else if points < 30 {
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "silverstar.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 1.0
                rewardSprite.scale = 0.7
                
                
            }
            else {
                let frame: CCSpriteFrame = CCSpriteFrame(imageNamed: "goldstar.png")
                rewardSprite.spriteFrame = frame
                rewardSprite.opacity = 1.0
                rewardSprite.scale = 0.7
                
                
            }


        
//        var splash2: CCParticleSystem = CCBReader.load("Shatter") as! Shatter
//        // make the particle effect clean itself up, once it is completed
//        splash2.autoRemoveOnFinish = true;
//     
////            splash2.colorRGBA = UIColor(red: CGFloat(202.0/255.0), green: CGFloat(228.0/255.0), blue: CGFloat(230.0/255.0), alpha: CGFloat(1.0))
//        //    CGPoint partPos = [ _gameplayNode convertToNodeSpace:(nodeA.position) ];
//        // place the particle effect on the seals position
//        circle.position = circle.positionInPoints;
//        // add the particle effect to the scene
//        circle.parent.addChild(splash2)
            music.stopBg()

        
        
        }
        return true

    }
    
    func credits(){
//        mixpanel.track("credits")
        self.animationManager.runAnimationsForSequenceNamed("credits")

    }
    
    func rate(){
//        mixpanel.track("rate")

        UIApplication.sharedApplication().openURL(NSURL(string:"https://itunes.apple.com/us/app/be-square/id1022407151?ls=1&mt=8")!)
    }
    
    func retry(){
//        mixpanel.track("retry")

        if musicActivated {
        music.playBg()
        }
    self.animationManager.runAnimationsForSequenceNamed("exitGameOver")
        delay(0.7){
        self.gameOver = false
        }
        reload = reloadMax
        reloadBar.scaleX = (reload/reloadMax)
//        iAdHandler.sharedInstance.displayBannerAd()


}

    override func update(delta: CCTime) {
        if (!gameOver && reload < reloadMax){
        reload+=20
        reloadBar.scaleX = (reload/reloadMax)
        }
        
//        oval.rotation = oval.rotation - 1
        
//        NSLog(String(stringInterpolationSegment: gameOver))
//        NSLog(String(stringInterpolationSegment: reload))
    }
    
    func spawnNewObstacle(){
//        obstacle.removeFromParent()
//        obstacle.position = obstaclePlacerNode.positionInPoints
//        gamePhysicsNode.addChild(obstacle)
        obstacle.setupRandomPosition()
//        obstacles.append(obstacle)
        
    }
    
    override func touchBegan(touch: CCTouch!, withEvent event: CCTouchEvent!){
        if (reload >= reloadMax) {
reload = 0
        spawnBlocks()
        }
    }

    
    func spawnBlocks()
    {

        if (!gameOver){
//            iAdHandler.sharedInstance.loadInterstitialAd()

        
        
        let random2 = arc4random_uniform(5)
        
//        if (random2 == 1 || random2 == 2 || random2 == 3 || random2 == 4){
//            NSLog("launching")
//            ammo++
//            var square = CCBReader.load("Square")
//            square.position = launchHere.positionInPoints
//            gamePhysicsNode.addChild(square)
//            //            var speed:UInt32 = arc4random(30)
////            if points < 10{
//            square.physicsBody.velocity.x = CGFloat(arc4random_uniform(UInt32(150))) + 80
//            NSLog(String(stringInterpolationSegment: square.physicsBody.velocity.x))
//
////            }
////            else if points < 20 {
//            
////            }
//            
//        }
//        else if (random2 == 0){
            ammo++
            NSLog("launching")
            let circle = CCBReader.load("Circle")
            circle.position = launchHere.positionInPoints
            gamePhysicsNode.addChild(circle)
            //            var speed = arc4random(UInt32((30))
            circle.physicsBody.velocity.y = -250
            NSLog(String(stringInterpolationSegment: circle.physicsBody.velocity.y))
//    }
            
            
    
    
    
    
    }
    }
    
    
}


//
//
//func tapsRemoveAds(){
//    NSLog("User requests to remove ads");
//
//    if SKPaymentQueue.canMakePayments(){
//        NSLog("User can make payments");
//
//        var productsRequest:SKProductsRequest  = SKProductsRequest.initialize() (NSSet.setWithObject:kRemoveAdsProductIdentifier)
//        productsRequest.delegate = self;
//        [productsRequest start];
//        
//    }
//    else{
//        NSLog(@"User cannot make payments due to parental controls");
//        //this is called the user cannot make payments, most likely due to parental controls
//    }
//    }
//    
//    - (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
//        SKProduct *validProduct = nil;
//        int count = [response.products count];
//        if(count > 0){
//            validProduct = [response.products objectAtIndex:0];
//            NSLog(@"Products Available!");
//            [self purchase:validProduct];
//        }
//        else if(!validProduct){
//            NSLog(@"No products available");
//            //this is called if your product id is not valid, this shouldn't be called unless that happens.
//        }
//        }
//        
//        - (void)purchase:(SKProduct *)product{
//            [mixpanel track:@"AdsRemoved"];
//            
//            SKPayment *payment = [SKPayment paymentWithProduct:product];
//            
//            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
//            [[SKPaymentQueue defaultQueue] addPayment:payment];
//            
//            areAdsRemoved = true;
//            [[NSUserDefaults standardUserDefaults] setBool:(areAdsRemoved) forKey:(@"areAdsRemoved")];
//            
//            }
//            
//            - (void) restore{
//                //this is called when the user restores purchases, you should hook this up to a button
//                [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
//                }
//                
//                - (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
//{
//    NSLog(@"received restored transactions: %i", queue.transactions.count);
//    for(SKPaymentTransaction *transaction in queue.transactions){
//        if(transaction.transactionState == SKPaymentTransactionStateRestored){
//            //called when the user successfully restores a purchase
//            NSLog(@"Transaction state -> Restored");
//            
//            [self doRemoveAds];
//            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
//            break;
//        }
//    }
//    }
//    
//    
//    - (void)doRemoveAds{
//        ADBannerView *banner;
//        [banner setAlpha:0];
//        areAdsRemoved = YES;
//        removeAdsButton.visible = false;
//        removeAdsButton.enabled = NO;
//        [[NSUserDefaults standardUserDefaults] setBool:areAdsRemoved forKey:@"areAdsRemoved"];
//        //use NSUserDefaults so that you can load whether or not they bought it
//        //it would be better to use KeyChain access, or something more secure
//        //to store the user data, because NSUserDefaults can be changed.
//        //You're average downloader won't be able to change it very easily, but
//        //it's still best to use something more secure than NSUserDefaults.
//        //For the purpose of this tutorial, though, we're going to use NSUserDefaults
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        }
//        
//        
//        
//        - (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
//            for(SKPaymentTransaction *transaction in transactions){
//                switch(transaction.transactionState){
//                case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
//                //called when the user is in the process of purchasing, do not add any of your own code here.
//                break;
//                case SKPaymentTransactionStatePurchased:
//                    //this is called when the user has successfully purchased the package (Cha-Ching!)
//                    [self doRemoveAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
//                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
//                    NSLog(@"Transaction state -> Purchased");
//                    break;
//                case SKPaymentTransactionStateRestored:
//                    NSLog(@"Transaction state -> Restored");
//                    //add the same code as you did from SKPaymentTransactionStatePurchased here
//                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
//                    break;
//                case SKPaymentTransactionStateFailed:
//                    //called when the transaction does not finish
//                    if(transaction.error.code == SKErrorPaymentCancelled){
//                        NSLog(@"Transaction state -> Cancelled");
//                        //the user cancelled the payment ;(
//                    }
//                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
//                    break;
//                }
//            }
//}











// MARK: Game Center Handling

extension MainScene: GKGameCenterControllerDelegate {
    
    func showLeaderboard() {
//        mixpanel.track("leaderboard")

        let viewController = CCDirector.sharedDirector().parentViewController!
        let gameCenterViewController = GKGameCenterViewController()
        gameCenterViewController.gameCenterDelegate = self
        viewController.presentViewController(gameCenterViewController, animated: true, completion: nil)
    }
    
    // Delegate methods
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
}






